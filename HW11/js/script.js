let number = +prompt('Enter quantity of rows');
const content = [];
  for(let i = 0; i < number; i++){
  let item = prompt(`Enter content of the ${i+1}_th row`);
  content[i] = item;
};
const createList = function(text){
  let li = document.createElement('li');
  li.innerHTML=`${text}`;
  return li;
};
const summonList = content.map(createList);
let ul = document.createElement('ul');
for(let key of summonList){
  ul.appendChild(key);
}
document.body.appendChild(ul);
setInterval(1000);
setTimeout(function(){
  ul.parentElement.removeChild(ul);
},10000);
