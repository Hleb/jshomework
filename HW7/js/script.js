function excludeBy(arr1, arr2, value) {
   let i = 0;
   var newArr = [];
   innerLoop:
       for (i; i < arr1.length; i++) {
              outLoop:
               for (let j = 0; j < arr2.length; j++) {
                  if (arr1[i][value] == arr2[j][value]) continue innerLoop;
                  else if (j == arr2.length-1 && arr1[i][value] !== arr2[j][value]) {
                   newArr.push(arr1[i]);
               }
               else continue outLoop;
           }
       }
   return newArr;
} ;

const usersList1 = [{
       name: "Petr",
       surname: "Nosov",
       gender: "male",
       age: 44
   },
   {
       name: "Ivan",
       surname: "Ivanov",
       gender: "male",
       age: 22
   },
   {
       name: "Dmitry",
       surname: "Ivanov",
       gender: "male",
       age: 14
   }
]
const usersList2 = [{
       name: "Alexandr",
       surname: "Pushkin",
       gender: "male",
       age: 33
   },
   {
       name: "Anna",
       surname: "Ivanova",
       gender: "female",
       age: 33
   },
   {
       name: "Ivan",
       surname: "Ivanov",
       gender: "male",
       age: 22
   }
]

console.log(excludeBy(usersList1, usersList2, "age"));
