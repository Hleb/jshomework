$(document).ready(function(){
    $("#btn").click(function(){
      $("#btn").hide();
      $("body").append("<input id='radiusInput' type='text' placeholder='Enter radius'>");
      $("body").append("<input id='colorInput' type='text' placeholder='Enter color'>");
      $("body").append("<button id='addCircle'>add circle</button>");
      $("#addCircle").click(function(){
        $("body").append(`<div id='circle'></div>`);
        $('#circle').css({
          width:`${+$("#radiusInput").val()}px`,
          height:`${+$("#radiusInput").val()}px`,
          'border-radius':'50%',
          'background-color':`${$("#colorInput").val()}`
        })
      });
    });

});
