function addInputs(){
 const radiusInput = document.createElement('input');
 const colorInput = document.createElement('input');
 const buttonAdd = document.createElement('button');
 radiusInput.placeholder = 'Enter radius';
 colorInput.placeholder = 'Enter collor';
 buttonAdd.id = 'addCircle';
 buttonAdd.innerText = 'add circle' ;
 document.body.appendChild(radiusInput);
 document.body.appendChild(colorInput);
 document.body.appendChild(buttonAdd);

 function addCircle(){
 let circle = document.createElement('div');
 circle.id = 'circle';
 circle.style.cssText = `
 background-color: ${colorInput.value};
 width: ${radiusInput.value}px;
 height: ${radiusInput.value}px;
 border-radius: ${radiusInput.value/2}px;
 `
 document.body.appendChild(circle);
 }
 document.getElementById("addCircle").addEventListener("click", addCircle);
}
document.getElementById("btn").addEventListener("click", addInputs);
document.getElementById("btn").addEventListener("click", function() {this.remove()});
