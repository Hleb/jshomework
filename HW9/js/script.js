function getAge(dateString) {
    let dates = dateString.split(".");
    let d = new Date();
    let birthday = dates[0];
    let birthmonth = dates[1];
    let birthyear = dates[2];
    let curday = d.getDate();
    let curmonth = d.getMonth()+1;
    let curyear = d.getFullYear();
    let age = curyear - birthyear;
    if((curmonth < birthmonth) || ((curmonth == birthmonth) && curday < birthday)){
        age--;
    }
    return age;
};
function getZodiac(dateString) {
    let list = [
        [1, 'What?'],
        [19, 'Capricorn'],
        [18, 'Aquarius'],
        [20, 'Pisces'],
        [19, 'Aries'],
        [20, 'Taurus'],
        [21, 'Gemini'],
        [22, 'Cancer'],
        [22, 'Leo'],
        [22, 'Virgo'],
        [22, 'Libra'],
        [22, 'Scorpio'],
        [21, 'Sagittarius']
    ];
    let dates = dateString.split(".");
    let birthday = dates[0];
    let birthmonth = dates[1];
    let birthyear = dates[2];
    let bm = birthmonth.split("0");
    if(bm[0]==0){
    if (birthday < 1 || birthday > 31) {
        bm = 0;
        birthrday = 0;
    }
    if (bm < 1 || bm > 12) {
        bm = 0;
        birthday = 0;
    }
    if (birthday > list[bm[1]][0]) {
      bm += 1;
    }
    if (bm > 12) {
    bm = 1;
    }
    return list[bm[1]][1];
  }
  else{
    if (birthday < 1 || birthday > 31) {
        birthmonth = 0;
        birthrday = 0;
    }
    if (birthmonth < 1 || birthmonth > 12) {
        birthmonth = 0;
        birthday = 0;
    }
    if (birthday > list[birthmonth][0]) {
      birthmonth += 1;
    }
    if (birthmonth > 12) {
    birthmonth = 1;
    }
    return list[birthmonth][1];
  }
}

let userDate = prompt('Enter your birthdate');
alert(`Your age is ${getAge(userDate)}`);
alert(`Zodiac - ${getZodiac(userDate)}`);
