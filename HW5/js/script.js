function objectClone (obj) {
	let copy = {};
		for (key in obj) {
			let newKey = obj[key];
			let currentProp;
		 if(typeof newKey == "object"){
			currentProp=objectClone(newKey)
		}
		else{
			currentProp=newKey;
		}
		copy[key]=currentProp;
		}
		return copy;


}
var menuSetup = {
  width: 300,
  height: 200,
  title: "Menu",
	adress:{
		house:43,
		street:"street"
	}
};

let obj2 = objectClone(menuSetup);
console.log(menuSetup)
console.log(obj2)
obj2.width = 400;
console.log(menuSetup)
console.log(obj2)
