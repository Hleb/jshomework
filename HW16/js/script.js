  let head = document.head;
  let link = document.createElement('link');
  link.rel = 'stylesheet';
  if (localStorage.getItem('themeStyle') === 'dark') {
    link.href = 'css/dark.css';
    document.getElementById('changeTheme').setAttribute('checked', true);
  }
  else {
    link.href = 'css/style.css';
  }
  head.appendChild(link);
  document.getElementById('changeTheme').addEventListener('change', ev => {
    let btn = ev.target;
    if (btn.checked) {
      link.href = 'css/dark.css';
      localStorage.setItem('themeStyle', 'dark');
    }
    else {
      link.href = 'css/style.css';
      localStorage.setItem('themeStyle', 'standart'); 
    }
  });
