let time = 0;
let stream = 0;

function reset() {
  location.reload();
};

function startStop() {
  if (stream == 0){
    stream = 1;
    increment();
  }else{
    stream = 0;
  };
};

function increment() {
  if (stream == 1){
    setTimeout(function(){
      time++;
      let hrs = Math.floor(time/10/60/60);
      let min = Math.floor(time/10/60);
      let sec = Math.floor(time/10);
      let ms = time%10;

      if (hrs < 10){
        hrs = '0' + hrs;
      }
      if (min < 10){
        min = '0' + min ;
      }
      if (sec < 10){
        sec = '0' + sec ;
      };
      document.getElementById('timer').value  = hrs + ':' + min + ':' + sec + ':'+'0' + ms ;
      increment();
    },100);
  }
};
document.getElementById("startStop").addEventListener("click", startStop);
document.getElementById("reset").addEventListener("click", reset);
